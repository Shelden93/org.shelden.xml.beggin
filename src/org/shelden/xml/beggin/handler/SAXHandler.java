package org.shelden.xml.beggin.handler;

import org.shelden.xml.beggin.model.Employee;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {

	private static final String ATTR_ID = "id";

	private static final String ELEMENT_FIRST_NAME = "firstName";
	private static final String ELEMENT_LAST_NAME = "lastName";
	private static final String ELEMENT_EMPLOYEE = "employee";
	private static final String ELEMENT_LOCATION = "location";

	private List<Employee> employees = new ArrayList<>();
	private Employee employee = null;
	private String content = null;

	@Override
	public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {

		switch (qName) {
			case ELEMENT_EMPLOYEE:
				employee = new Employee();
				employee.setId(attributes.getValue(ATTR_ID));
				break;
		}
	}

	@Override
	public void endElement(final String uri, final String localName, final String qName) throws SAXException {

		switch (qName) {
			case ELEMENT_EMPLOYEE:
				employees.add(employee);
				break;
			case ELEMENT_FIRST_NAME:
				employee.setFirstName(content);
				break;
			case ELEMENT_LAST_NAME:
				employee.setLastName(content);
				break;
			case ELEMENT_LOCATION:
				employee.setLocation(content);
				break;
		}
	}

	@Override
	public void characters(final char[] ch, final int start, final int length) throws SAXException {
		content = String.copyValueOf(ch, start, length).trim();
	}

	public List<Employee> getEmployees() {
		return employees;
	}
}
