package org.shelden.xml.beggin.handler;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.ProcessingInstruction;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Stack;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class SAXtoDOMHandler extends DefaultHandler implements LexicalHandler {

	private static final String XMLNS_URI = "http://www.w3.org/2000/xmlns/";
	private static final String XMLNS_STRING = "xmlns:";
	private static final String XMLNS_PREFIX = "xmlns";

	private static final String EMPTY_STRING = "";

	private Node root = null;
	private Document document = null;
	private Stack<Node> nodeStack = new Stack<>();
	private Vector<String> namespaces = null;

	public SAXtoDOMHandler() throws ParserConfigurationException {

		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		final DocumentBuilder builder = factory.newDocumentBuilder();

		this.document = builder.newDocument();
		this.root = this.document;
	}

	public SAXtoDOMHandler(final Node root) throws ParserConfigurationException {
		this.root = root;

		if (root instanceof Document) {
			this.document = (Document) root;
		} else if (root != null) {
			this.document = root.getOwnerDocument();
		} else {

			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder builder = factory.newDocumentBuilder();

			this.document = builder.newDocument();
			this.root = document;
		}
	}

	public Node getRootNode() {
		return root;
	}

	public Document getDocument() {
		return document;
	}

	@Override
	public void characters(final char[] ch, final int start, final int length) throws SAXException {
		final Node last = nodeStack.peek();

		// No text nodes can be children of root (DOM006 exception)
		if (last != document) {
			final String text = String.copyValueOf(ch, start, length).trim();
			last.appendChild(document.createTextNode(text));
		}
	}

	@Override
	public void startDocument() {
		nodeStack.push(root);
	}

	@Override
	public void endDocument() {
		nodeStack.pop();
	}

	@Override
	public void startElement(final String namespace, final String localName, final String qName, final Attributes attrs) {
		final Element tmp = document.createElementNS(namespace, qName);

		if (namespaces != null) {

			for (int i = 0; i < namespaces.size(); i++) {

				final String prefix = namespaces.elementAt(i++);

				if (prefix == null || prefix.equals(EMPTY_STRING)) {
					tmp.setAttributeNS(XMLNS_URI, XMLNS_PREFIX, namespaces.elementAt(i));
				} else {
					tmp.setAttributeNS(XMLNS_URI, XMLNS_STRING + prefix, namespaces.elementAt(i));
				}
			}

			namespaces.clear();
		}

		for (int i = 0; i < attrs.getLength(); i++) {

			if (attrs.getLocalName(i) == null) {
				tmp.setAttribute(attrs.getQName(i), attrs.getValue(i));
			} else {
				tmp.setAttributeNS(attrs.getURI(i), attrs.getQName(i), attrs.getValue(i));
			}
		}

		final Node last = nodeStack.peek();
		last.appendChild(tmp);

		nodeStack.push(tmp);
	}

	@Override
	public void endElement(final String namespace, final String localName, final String qName) {
		nodeStack.pop();
	}

	@Override
	public void startPrefixMapping(final String prefix, final String uri) throws SAXException {

		if (namespaces == null) {
			namespaces = new Vector<>(2);
		}

		namespaces.addElement(prefix);
		namespaces.addElement(uri);
	}

	@Override
	public void endPrefixMapping(final String prefix) throws SAXException {

	}

	@Override
	public void ignorableWhitespace(final char[] ch, final int start, final int length) throws SAXException {

	}

	@Override
	public void processingInstruction(final String target, final String data) {
		final Node last = nodeStack.peek();

		final ProcessingInstruction pi = document.createProcessingInstruction(target, data);
		if (pi != null) last.appendChild(pi);
	}

	@Override
	public void setDocumentLocator(final Locator locator) {

	}

	@Override
	public void skippedEntity(final String name) {

	}

	@Override
	public void comment(final char[] ch, final int start, final int length) {
		final Node last = nodeStack.peek();

		final Comment comment = document.createComment(new String(ch, start, length));
		if (comment != null) last.appendChild(comment);
	}

	@Override
	public void startCDATA() {

	}

	@Override
	public void endCDATA() {

	}

	@Override
	public void startEntity(final String name) {

	}

	@Override
	public void endEntity(final String name) {

	}

	@Override
	public void startDTD(final String name, final String publicId, final String systemId) throws SAXException {

	}

	@Override
	public void endDTD() {

	}
}
