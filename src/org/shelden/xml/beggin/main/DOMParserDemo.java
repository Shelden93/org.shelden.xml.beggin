package org.shelden.xml.beggin.main;

import org.shelden.xml.beggin.model.Employee;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class DOMParserDemo {

	private static final String ATTR_ID = "id";
	private static final String ELEMENT_FIRST_NAME = "firstName";
	private static final String ELEMENT_LAST_NAME = "lastName";
	private static final String ELEMENT_LOCATION = "location";

	public static void main(String[] args) throws Exception {

		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		final DocumentBuilder builder = factory.newDocumentBuilder();

		Document document = null;
		try (InputStream in = DOMParserDemo.class.getResourceAsStream("/test.xml")) {
			document = builder.parse(in);
		} catch (Exception e) {
			e.printStackTrace();
		}

		printEmployeesFromDocument(document);
	}

	static void printEmployeesFromDocument(final Document document) {

		if (document == null) {
			throw new RuntimeException("Document is null...");
		}

		final List<Employee> employees = new ArrayList<>();

		final NodeList nodeList = document.getDocumentElement().getChildNodes();

		for (int i = 0; i < nodeList.getLength(); i++) {

			final Node node = nodeList.item(i);

			if (node instanceof Element) {

				final Employee emp = new Employee();

				final NamedNodeMap attributes = node.getAttributes();
				emp.setId(attributes.getNamedItem(ATTR_ID).getNodeValue());

				final NodeList childNodes = node.getChildNodes();

				for (int j = 0; j < childNodes.getLength(); j++) {

					final Node cNode = childNodes.item(j);

					if (cNode instanceof Element) {

						final String content = cNode.getTextContent();

						switch (cNode.getNodeName()) {
							case ELEMENT_FIRST_NAME:
								emp.setFirstName(content);
								break;
							case ELEMENT_LAST_NAME:
								emp.setLastName(content);
								break;
							case ELEMENT_LOCATION:
								emp.setLocation(content);
								break;
						}
					}
				}
				employees.add(emp);
			}
		}

		employees.forEach(System.out::println);
	}
}