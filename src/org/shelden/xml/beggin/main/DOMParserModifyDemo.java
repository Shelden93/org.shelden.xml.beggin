package org.shelden.xml.beggin.main;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class DOMParserModifyDemo {

	public static void main(String[] args) throws Exception {

		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		final DocumentBuilder builder = factory.newDocumentBuilder();

		Document document = null;
		try (InputStream in = DOMParserDemo.class.getResourceAsStream("/test.xml")) {
			document = builder.parse(in);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (document == null) {
			throw new RuntimeException("Document is null...");
		}

		System.out.println("Original file source:");
		printResultDocument(document);
		System.out.println();
		System.out.println();

		final NodeList locations = document.getElementsByTagName("location");

		while (locations.getLength() > 0){
			final Node location = locations.item(0);

			final Node parentNode = location.getParentNode();
			parentNode.removeChild(location);
		}

		System.out.println("Modify file source:");
		printResultDocument(document);
		System.out.println();
	}

	private static void printResultDocument(final Document document) {

		final TransformerFactory transFactory = TransformerFactory.newInstance();

		Transformer transformer = null;
		try {
			transformer = transFactory.newTransformer();
			transformer.transform(new DOMSource(document), new StreamResult(System.out));
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}
}
