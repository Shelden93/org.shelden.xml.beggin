package org.shelden.xml.beggin.main;

import org.shelden.xml.beggin.handler.SAXHandler;

import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class SAXParserDemo {

	public static void main(String[] args) throws Exception {

		final SAXParserFactory parserFactor = SAXParserFactory.newInstance();
		final SAXParser parser = parserFactor.newSAXParser();

		final SAXHandler handler = new SAXHandler();

		try (InputStream in = DOMParserDemo.class.getResourceAsStream("/test.xml")) {
			parser.parse(in, handler);
		} catch (Exception e) {
			e.printStackTrace();
		}

		handler.getEmployees().forEach(System.out::println);
	}
}