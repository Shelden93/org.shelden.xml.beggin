package org.shelden.xml.beggin.main;

import org.shelden.xml.beggin.handler.SAXtoDOMHandler;

import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import static org.shelden.xml.beggin.main.DOMParserDemo.printEmployeesFromDocument;

public class SAXtoDOMParserDemo {

	public static void main(String[] args) throws Exception {

		final SAXParserFactory parserFactor = SAXParserFactory.newInstance();
		final SAXParser parser = parserFactor.newSAXParser();

		final SAXtoDOMHandler handler = new SAXtoDOMHandler();

		try (InputStream in = DOMParserDemo.class.getResourceAsStream("/test.xml")) {
			parser.parse(in, handler);
		} catch (Exception e) {
			e.printStackTrace();
		}

		printEmployeesFromDocument(handler.getDocument());
	}
}